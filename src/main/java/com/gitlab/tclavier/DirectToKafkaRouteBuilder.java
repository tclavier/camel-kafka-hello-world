package com.gitlab.tclavier;

import org.apache.camel.builder.RouteBuilder;

public class DirectToKafkaRouteBuilder extends RouteBuilder {

    private String topic;

    @Override
    public void configure() throws Exception {

        topic = "my-topic";
        String topicName = "topic=" + topic;
        String kafkaServer = "kafka:localhost:9093";
        String zooKeeperHost = "zookeeperHost=localhost&zookeeperPort=2181";
        String serializerClass = "serializerClass=kafka.serializer.StringEncoder";

        String toKafka = kafkaServer + "?" + topicName + "&" + zooKeeperHost + "&" + serializerClass;

        from("direct:a").to(toKafka);
    }
}

