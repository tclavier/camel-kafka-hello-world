package com.gitlab.tclavier;


import org.apache.camel.CamelContext;
import org.apache.camel.impl.DefaultCamelContext;

public class MainApp {

    public static void main(String[] args) {
        FileSystemToKafkaRouteBuilder fs2kafka = new FileSystemToKafkaRouteBuilder();
        DirectToKafkaRouteBuilder direct2kafka = new DirectToKafkaRouteBuilder();
        CamelContext ctx = new DefaultCamelContext();
        try {
            ctx.addRoutes(fs2kafka);
            ctx.addRoutes(direct2kafka);
            ctx.start();
            Thread.sleep(5 * 60 * 1000);
            ctx.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
